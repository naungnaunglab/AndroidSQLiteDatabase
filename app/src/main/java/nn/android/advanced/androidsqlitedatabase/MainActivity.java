package nn.android.advanced.androidsqlitedatabase;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase db;
    TextView tv;
    EditText et1, et2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize all view objects
        tv = (TextView)findViewById(R.id.textView1);
        et1 = (EditText)findViewById(R.id.editText1);
        et2 = (EditText)findViewById(R.id.editText2);

        // Create database if not already exist
        db = openOrCreateDatabase("Mydb", MODE_PRIVATE, null);

        // Create new table if not already exist
        db.execSQL("create table if not exists mytable(f_name varchar, l_name varchar)");
    }

    public void insert(View v) {
        String f_name = et1.getText().toString();
        String l_name = et2.getText().toString();

        et1.setText("");
        et2.setText("");

        // Insert data into able
        db.execSQL("insert into mytable values('"+ f_name +"', '"+ l_name + "')");

        // Display Toast
        Toast.makeText(this, "values inserted successfully.", Toast.LENGTH_SHORT).show();
    }

    // This method will call when we click on display button
    public void display(View v) {

        // Use cursor to keep all data
        // Cursor can keep data of any data type
        Cursor c = db.rawQuery("select * from mytable", null);
        tv.setText("");

        // Move cursor to first position
        c.moveToFirst();

        // Fetch all data one by one
        do {
            // We can use c.getString(0) here
            // or We can get data using column index
            String f_name = c.getString(c.getColumnIndex("f_name"));
            String l_name = c.getString(1);

            // Display on text view
            tv.append("First Name : " + f_name + " and Last Name : " + l_name + "\n");
        }
        while (c.moveToNext());
    }
}
